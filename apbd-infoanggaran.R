#analisa pembengkakan anggaran belanja dana sosial dan hibah untuk petahana
require(XML)||install.packages("XML")
require(dplyr)||install.packages("dplyr")
require(readODS)||install.packages("readODS")

#list petahana
a=read.csv(paste0("paslonpilkada",format(Sys.time(),"%Y%m%d"),".csv"))
b=grep("^gubernur",a$pekerjaan,ignore.case = T)
c=grep("^bupati",a$pekerjaan,ignore.case = T)
d=grep("^walikota",a$pekerjaan,ignore.case = T)
petahana=rbind(a[b,],a[c,],a[d,])
notpet=grep("^wakil",petahana$jabatan)
petahana=petahana[-notpet,]
write.csv(petahana,"petahana.csv")
#
petahanalist=c(
       'provinsi jambi'=15,
       'kabupaten agam'=1307,
       'kabupaten bandung'=3204,
       'kabupaten banggai'=7202,
       'provinsi sumatera barat'=13,
       'kabupaten bangka barat'=1903,
       'kabupaten bangka tengah'=1904,
       'kabupaten bangka selatan'=1905,
       'kabupaten malang'=3507,
       'kabupaten banyuwangi'=3510,
       'kabupaten belitung timur'=1906,
       'kabupaten bengkalis'=1408,
       'kabupaten bengkayang'=1602,
       'kabupaten batanghari'=1504,
       'kabupaten bengkulu selatan'=1701,
       'kota manado'=7171,
       'kota tomohon'=7173,
       'kabupaten minahasa selatan'=7105,
       'kabupaten minahasa utara'=7106,
       'kabupaten bolaang mongondow selatan'=7110,
       'kota tangerang selatan'=3674,
       'kota bukit tinggi'=1375,
       'kabupaten pasaman'=1309,
       'kota solok'=1372,
       'kota malang'=3573,
       'kota surabaya'=3578,
       'kota surakarta'=3372,
       'kota magelang'=3371,
       'kota palu'=7271,
       'kota ternate'=8271,
       'kota balikpapan'=6471,
       'kota samarinda'=6472,
       'kota bontang'=6474,
       'kota pematang siantar'=1273,
       'kota sibolga'=1271,
       'kota binjai'=1276,
       'kota gunungsitoli'=1278,
       'kota bandarlampung'=1871,
       'kota blitar'=3572,
       'kota sungaipenuh'=1572,
       'kota mataram'=5271,
       'kota banjarbaru'=6372,
       'provinsi kepulauan riau'=21,
       'kabupaten yalimo'=9432,
       'kabupaten sambas'=6101,
       'kabupaten toli toli'=7206,
       'kabupaten toraja utara'=7326,
       'kabupaten tana toraja'=7318,
       'kabupaten luwu utara'=7322,
       'kabupaten maros'=7308,
       'kabupaten pangkep'=7309,
       'kabupaten labuhanbatu'=1207,
       'kabupaten labuhanbatu selatan'=1222,
       'kabupaten labuhanbatu utara'=1223,
       'kabupaten karo'=1211,
       'kabupaten nias'=1201,
       'kabupaten nias barat'=1225,
       'kabupaten nias selatan'=1214,
       'kabupaten nias utara'=1224,
       'kabupaten waykanan'=1807,
       'kabupaten lampung selatan'=1803,
       'kabupaten lampung timur'=1804,
       'kabupaten pakpak bharat'=1216,
       'kabupaten lamongan'=3524,
       'kabupaten waropen'=9426,
       'kabupaten supiori'=9427,
       'kabupaten pacitan'=3501,
       'kabupaten ogan komering ulu'=1601,
       'kabupaten lombok tengah'=5202,
       'kabupaten lombok utara'=5208,
       'kabupaten dompu'=5205,
       'kabupaten jembrana'=5101,
       'kabupaten lebong'=1707,
       'kabupaten malinau'=6501,
       'kabupaten bima'=5206,
       'kabupaten blora'=3316,
       'kabupaten bolaang mongondow selatan'=7110,
       'kabupaten bone bolango'=7504,
       'kabupaten boven digoel'=9413,
       'kabupaten bungo'=1509,
       'kabupaten buru selatan'=8109,
       'kabupaten demak'=3321,
       'kabupaten dharmasraya'=1311,
       'kabupaten halmahera timur'=8206,
       'kabupaten indragiri hulu'=1402,
       'kabupaten indramayu'=3212,
       'kabupaten kapuas hulu'=61,
       'kabupaten keerom'=9420,
       'kabupaten kendal'=3324,
       'kabupaten konawe utara'=7410,
       'kabupaten kotabaru'=6302,
       'kabupaten kotawaringin timur'=6202,
       'kabupaten kutai kartanegara'=6403,
       'kabupaten maluku barat daya'=8108,
       'kabupaten mamberamo raya'=9428,
       'kabupaten mamuju utara'=7605,
       'kabupaten mandailing natal'=1202,
       'kabupaten melawi'=6110,
       'kabupaten merauke'=9401,
       'kabupaten mojokerto'=3516,
       'kabupaten muna'=7402,
       'kabupaten ngada'=5312,
       'kabupaten ngawi'=3521,
       'kabupaten nunukan'=6504,
       'kabupaten padang pariaman'=1306,
       'kabupaten pohuwato'=7503,
       'kabupaten pasaman'=1309,
       'kabupaten pelalawan'=1404,
       'kabupaten pemalang'=3327,
       'kabupaten pesawaran'=1809,
       'kabupaten seluma'=1705,
       'kabupaten siak'=1405,
       'kabupaten sidoarjo'=3515,
       'kabupaten sijunjung'=1304,
       'kabupaten simalungun'=1209,
       'kabupaten situbondo'=3512,
       'kabupaten sleman'=3404,
       'kabupaten sragen'=3314,
       'kabupaten sukoharjo'=3311,
       'kabupaten sumenep'=3529,
       'kabupaten tabanan'=5102,
       'kabupaten tanah bumbu'=6310,
       'kabupaten tapanuli selatan'=1203
       #'kabupaten kutai timur'=6404,
       #'kabupaten solok selatan'=,
       #manokwari=9105,belum ada data `15
       #rokanhilir=1409, belum ada data `15
       #'kabupaten natuna'=tidak ditemukan,
       )

xx=data.frame(stringsAsFactors = FALSE)
for (i in 1:length(petahanalist)){
x=readHTMLTable(paste0("http://apps.info-anggaran.com/index.php/ourdata/datadetail/2014/m/",petahanalist[i]))
y=readHTMLTable(paste0("http://apps.info-anggaran.com/index.php/ourdata/datadetail/2015/m/",petahanalist[i]))
a=as.data.frame(x)
colnames(a)=c("uraian","alokasi")
a=a[-1,]
a$alokasi=gsub("Rp.","",a$alokasi)
a$alokasi=gsub("\\.","",a$alokasi)
a$alokasi=as.numeric(a$alokasi)
#a$alokasi=a$alokasi*1000000

b=as.data.frame(y)
colnames(b)=c("uraian","alokasi")
b=b[-1,]
b$alokasi=gsub("Rp.","",b$alokasi)
b$alokasi=gsub("\\.","",b$alokasi)
b$alokasi=as.numeric(b$alokasi)
#b$alokasi=b$alokasi*1000000

c=cbind(a,b$alokasi)
colnames(c)=c("uraian","2014","2015")

xx=rbind(xx,c(c[18,2],c[18,3],c[22,2]+c[23,2],c[22,3]+c[23,3],c[18,3]-c[18,2],(c[22,3]+c[23,3])-(c[22,2]+c[23,2])))

}
xxx=as.data.frame(names(petahanalist))
xx=cbind(xxx,xx)
colnames(xx)=c("daerah","2014T","2015T","2014B_H","2015B_H","bengkakT","bengkakB_H")
datapembengkakan=xx
datapembengkakan=mutate(datapembengkakan,
                        presentaseT=round(
                          datapembengkakan$bengkakT/datapembengkakan$`2014T`*100,2
                          ),
                        persentaseB_H=round(
                          datapembengkakan$bengkakB_H/datapembengkakan$`2014B_H`*100,2
                          )
                                         )
colnames(datapembengkakan)=c("Daerah","Total Belanja 2014","Total Belanja 2015","Belanja Bansos + Hibah 2014","Belanja Bansos + Hibah 2015","Pembengkakan Total","Pembengkakan Bansos + Hibah","Persentase Total","Persentase Bansos + Hibah")
#rm(list=setdiff(ls(), "datapembengkakan"))

write.csv(datapembengkakan,"dataapbdpetahana.csv")
