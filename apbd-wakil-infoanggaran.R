#analisa pembengkakan anggaran belanja dana sosial dan hibah untuk petahana
require(XML)||install.packages("XML")
require(dplyr)||install.packages("dplyr")
require(readODS)||install.packages("readODS")

#list petahana
a=read.csv(paste0("paslonpilkada",format(Sys.time(),"%Y%m%d"),".csv"))
b=grep("^wakil gubernur",a$pekerjaan,ignore.case = T)
c=grep("^wakil bupati",a$pekerjaan,ignore.case = T)
d=grep("^wakil walikota",a$pekerjaan,ignore.case = T)
wakpet=rbind(a[b,],a[c,],a[d,])
notpet=grep("^wakil",wakpet$jabatan)
wakpet=wakpet[-notpet,]
write.csv(wakpet,"wakpet.csv")


#list untuk cek anggaran
petahanalist=c(
  'kabupaten agam'=1307,
  'kabupaten bangka barat'=1903,
  'kabupaten bangka tengah'=1904,
  'kabupaten blora'=3316,
  'kabupaten bungo'=1509,
  'kabupaten demak'=3321,
  'kabupaten konawe utara'=7410,
  'kabupaten kotabaru'=6302,
  'kabupaten labuhanbatu utara'=1223,
  'kabupaten lombok utara'=5208,
  'kabupaten luwu utara'=7322,
  'kabupaten mamberamo raya'=9428,
  'kabupaten melawi'=6110,
  'kabupaten minahasa utara'=7106,
  'kabupaten mojokerto'=3516,
  'kabupaten pangkep'=7309,
  'kabupaten pemalang'=3327,
  'kabupaten seluma'=1705,
  'kabupaten sidoarjo'=3515,
  'kabupaten sijunjung'=1304,
  'kabupaten simalungun'=1209,
  'kabupaten sleman'=3404,
  'kabupaten tapanuli selatan'=1203,
  'kabupaten toli toli'=7206,
  'kabupaten waropen'=9426,
  'kabupaten siak'=1405,
  'kabupaten banyuwangi'=3510,
  'kabupaten bolaang mongondow selatan'=7110,
  'kabupaten bone bolango'=7504,
  'kabupaten halmahera timur'=8206,
  'kabupaten indramayu'=3212,
  'kabupaten jembrana'=5101,
  'kabupaten keerom'=9420,
  'kabupaten kotawaringin timur'=6202,
  'kabupaten lampung selatan'=1803,
  'kabupaten malinau'=6501,
  'kabupaten mamuju utara'=7605,
  'kabupaten mandailing natal'=1202,
  'kabupaten muna'=7402,
  'kabupaten ngawi'=3521,
  'kabupaten nias'=1201,
  'kabupaten pasaman'=1309,
  'kabupaten tabanan'=5102,
  'kabupaten toraja utara'=7326,
  'kota balikpapan'=6471,
  'kota bandarlampung'=1871,
  'kota bukit tinggi'=1375,
  'kota magelang'=3371,
  'kota manado'=7171,
  'kota solok'=1372,
  'kota binjai'=1276,
  'kota bontang'=6474,
  'kota mataram'=5271,
  'kota samarinda'=6472,
  'kota surabaya'=3578,
  'kota surakarta'=3372,
  'kota tangerang selatan'=3674,
  'kota ternate'=8271,
  'provinsi kepulauan riau'=21,
  'provinsi sumatera barat'=13,
  'provinsi jambi'=15
  
  
  #'kabupaten kutai timur'=6404,belum ada data `15
  #'kabupaten solok selatan'=,
  #manokwari=9105,belum ada data `15
  #rokanhilir=1409, belum ada data `15
  #'kabupaten natuna'=tidak ditemukan,
)

xx=data.frame(stringsAsFactors = FALSE)
for (i in 1:length(petahanalist)){
  x=readHTMLTable(paste0("http://apps.info-anggaran.com/index.php/ourdata/datadetail/2014/m/",petahanalist[i]))
  y=readHTMLTable(paste0("http://apps.info-anggaran.com/index.php/ourdata/datadetail/2015/m/",petahanalist[i]))
  a=as.data.frame(x)
  colnames(a)=c("uraian","alokasi")
  a=a[-1,]
  a$alokasi=gsub("Rp.","",a$alokasi)
  a$alokasi=gsub("\\.","",a$alokasi)
  a$alokasi=as.numeric(a$alokasi)
  #a$alokasi=a$alokasi*1000000
  
  b=as.data.frame(y)
  colnames(b)=c("uraian","alokasi")
  b=b[-1,]
  b$alokasi=gsub("Rp.","",b$alokasi)
  b$alokasi=gsub("\\.","",b$alokasi)
  b$alokasi=as.numeric(b$alokasi)
  #b$alokasi=b$alokasi*1000000
  
  c=cbind(a,b$alokasi)
  colnames(c)=c("uraian","2014","2015")
  
  xx=rbind(xx,c(c[18,2],c[18,3],c[22,2]+c[23,2],c[22,3]+c[23,3],c[18,3]-c[18,2],(c[22,3]+c[23,3])-(c[22,2]+c[23,2])))
  
}
xxx=as.data.frame(names(petahanalist))
xx=cbind(xxx,xx)
colnames(xx)=c("daerah","2014T","2015T","2014B_H","2015B_H","bengkakT","bengkakB_H")
datapembengkakan=xx
datapembengkakan=mutate(datapembengkakan,
                        presentaseT=round(
                          datapembengkakan$bengkakT/datapembengkakan$`2014T`*100,2
                        ),
                        persentaseB_H=round(
                          datapembengkakan$bengkakB_H/datapembengkakan$`2014B_H`*100,2
                        )
)
colnames(datapembengkakan)=c("Daerah","Total Belanja 2014","Total Belanja 2015","Belanja Bansos + Hibah 2014","Belanja Bansos + Hibah 2015","Pembengkakan Total","Pembengkakan Bansos + Hibah","Persentase Total","Persentase Bansos + Hibah")
#rm(list=setdiff(ls(), "datapembengkakan"))

write.csv(datapembengkakan,"dataapbdwakpet.csv")

