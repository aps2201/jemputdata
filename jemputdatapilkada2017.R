#load semua library -----
require(dplyr)||install.packages("dplyr")
require(plyr)||install.packages("plyr")
require(data.table)||install.packages("data.table")
require(XML)||install.packages("XML")
require(jsonlite)||install.packages("jsonlite")
require(stringr)||install.packages("stringr")


# Tahap I: data peserta tetap ---------------------------------------------
paslon = fromJSON("https://pilkada2017.kpu.go.id/paslon/listPaslonPenetapan.json",flatten = T)[1][[1]]
write.csv(paslon,paste0("paslon2017.csv",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)
hasil = fromJSON("https://pilkada2017.kpu.go.id/api/1/hasil.json",flatten = T)
write.csv(hasil,paste0("hasilpilkada",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)
hasil2 = fromJSON("https://pilkada2017.kpu.go.id/api/2/hasil.json",flatten = T)
write.csv(hasil,paste0("hasilpilkada2",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)

# Group dan hitung
hasil_sum = hasil %>% group_by(idWilayah) %>% mutate_(persenSuara = "jumlahSuara/sum(jumlahSuara)*100")
write.csv(hasil_sum,paste0("hasilpilkada_",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)
hasil_sum2 = hasil2 %>% group_by(idWilayah) %>% mutate_(persenSuara = "jumlahSuara/sum(jumlahSuara)*100")
write.csv(hasil_sum2,paste0("hasilpilkada2_",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)
# Tahap II: rincian peserta tetap ---------------------------------------------
#bikin list url dan tentukan batas atas dan bawah html
url_list = paste0("https://pilkada2017.kpu.go.id/paslon/",paslon$urlDetailPaslon)
start1_html =  294
end1_html = 299
start1_html = 310
end2_html = 315
paslonpilkada2017 = c()
for (i in 1:length(url_list)){
  paslonpilkada2017 =  c(paslonpilkada2017,list(readLines(url_list[i])))
}

# final -------------------------------------------------------------------
#gabung gabung
paslonpilkada2017kepala = c()
paslonpilkada2017wakil = c()
for (i in 1:length(url_list)){
paslonpilkada2017kepala = c(paslonpilkada2017kepala,list(paslonpilkada2017[[i]][start1_html:end1_html]))
paslonpilkada2017wakil = c(paslonpilkada2017wakil,list(paslonpilkada2017[[i]][start2_html:end2_html]))
}
paslonpilkada2017kepala = as.data.frame(t(as.data.frame(paslonpilkada2017kepala)))
paslonpilkada2017kepala = paslonpilkada2017kepala[,-2]
rownames(paslonpilkada2017kepala) <- 1:nrow(paslonpilkada2017kepala) 
colnames(paslonpilkada2017kepala) <- c("nama","gender","ttl","pekerjaan","alamat")
paslonpilkada2017kepala = mutate(paslonpilkada2017kepala,id = paslon$id)
paslonpilkada2017kepala = mutate(paslonpilkada2017kepala,jabatan = "kepala")


paslonpilkada2017wakil = as.data.frame(t(as.data.frame(paslonpilkada2017wakil)))
paslonpilkada2017wakil = paslonpilkada2017wakil[,-2]
rownames(paslonpilkada2017wakil) <- 1:nrow(paslonpilkada2017wakil) 
colnames(paslonpilkada2017wakil) <- c("nama","gender","ttl","pekerjaan","alamat")
paslonpilkada2017wakil = mutate(paslonpilkada2017wakil,id = paslon$id)
paslonpilkada2017wakil = mutate(paslonpilkada2017wakil,jabatan = "wakil")

paslonpilkada2017 = rbind(paslonpilkada2017kepala,paslonpilkada2017wakil)
paslonpilkada2017 = left_join(paslonpilkada2017,paslon,by ="id")

#bersih-bersih
rm(list=setdiff(ls(), "paslonpilkada2017"))

##petahana
paslonpilkada2017 = mutate(paslonpilkada2017,petahana = ifelse(grepl("petahana",paslonpilkada2017$pekerjaanKd)&grepl("kepala",paslonpilkada2017$jabatan),"petahana",ifelse(grepl("petahana",paslonpilkada2017$pekerjaanWkd)&grepl("wakil",paslonpilkada2017$jabatan),"petahana","-")))

##partai
paslonpilkada2017[,19] = gsub("</li><li>","-",paslonpilkada2017[,19])
paslonpilkada2017 = mutate(paslonpilkada2017,partai = ifelse(paslonpilkada2017[,19]!="",str_count(paslonpilkada2017[,19],"-")+1,0))

#rapih-rapih
for (i in 1:5){
paslonpilkada2017[,i] = gsub("<(.*?)>|:|Ttl|Alamat|Pekerjaan|Gender","",paslonpilkada2017[,i])
paslonpilkada2017[,i] = trimws(paslonpilkada2017[,i])
}

paslonpilkada2017 = paslonpilkada2017[,-c(12:17)]

##cleaning partai 
paslonpilkada2017[,13] = gsub("<(.*?)>","",paslonpilkada2017[,13])


#output
write.csv(paslonpilkada2017,paste0("paslonpilkada",format(Sys.time(),"%Y%m%d"),".csv"),row.names = F)

#POST =================
paslonpilkada2017 = read.csv(hasilpilkada_20170425.csv)
paslonpilkada2017$namaWilayah = as.character(paslonpilkada2017$namaWilayah)
longlat =  read.csv("simplemaps-worldcities-basic.csv",stringsAsFactors = F)
longlat$province = as.character(longlat$province)
longlat$province = toupper(longlat$province)
x = left_join(paslonpilkada2017,longlat,by = c("namaWilayah" = "province"))
