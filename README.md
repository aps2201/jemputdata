# JemputData Repo


Repo ini berisikan [R](https://www.rstudio.com/products/rstudio/download/) script yang digunakan untuk scraping, modifikasi, dan analisa data yang memiliki kepentingan publik silakan dibagipakai.


Pada tahun 2015 inisiatif JemputData dibentuk oleh Andaru Pramudito dan Resa Temaputra di bawah naungan [Public Virtue Institute](http://virtue.or.id) untuk membantu publik memiliki data untuk lebih memahami pejabat publik pada [pilkada serentak 2015](http://infopilkada.kpu.go.id/index.php) sebagai bagian dari pengembangan budaya politik sipil.
